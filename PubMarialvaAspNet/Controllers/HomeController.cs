﻿using PubMarialvaAspNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq;

namespace PubMarialvaAspNet.Controllers
{
    public class HomeController : Controller
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Local()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Reservas()
        {
           
            return View();
        }

        [HttpPost]
        public ActionResult Reservas(Reserva reserva)
        {
            if (ModelState.IsValid)
            {
                dc.Reservas.InsertOnSubmit(reserva);
            }

            try
            {
                dc.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }


        public ActionResult Menu()
        {
            //para passar mais do que 1 objecto
            List<object> refeicoes = new List<object>();

            refeicoes.Add(dc.Entradas.ToList());
            refeicoes.Add(dc.Acompanhamentos.ToList());
            refeicoes.Add(dc.Pratos.ToList());

            return View(refeicoes);
        }

   
    }
}